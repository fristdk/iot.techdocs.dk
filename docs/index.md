# Velkommen til IoT.techdocs.dk

Denne side er udarbejdet i forbindelse med undervisning til Amazon Webservices re/Start kursus i IoT 2021


## Introduktion
I de følgende øvelser og eksperimenter skal vi stifte bekendtskab med en Internet of Things (IoT) platform som er yderst udbredt i udviklings- og hobbykredse. Vi skal arbejde med en microcontroller (også tit forkortet som MCU, Microcontroller Unit) fra firmaet Espressif (1). Til dette kursus har jeg valgt, at vi anvender og arbejde med en MCU af typen ESP8266. MCU’en begyndte fra 2014 at få opmærksomhed som en hurtig udviklingsplatform,  i dag er der både hurtigere og bedre alternativer fra samme firma eller lignende. Men til vores formål som en indføring i Internet of Things og Rapid Prototyping, tjener den stadig et værdigt formål. Dette især fordi ESP8266 hurtigt blev en kerne i mange små og store projekter på mange niveauer og derfor findes der er meget stort fællesskab (Community) så det er tit og ofte let at finde hjælp og inspiration til løsninger online. En microcontroller MCU, er som udgangspunkt hovedkomponenten i et embedded IoT design.

De første øvelser er ligeledes opsat således, at der tilstræbes en indføring i basis terminologi brugt indenfor elektronik. Vi skal kigge på at få opsat et udviklingsmiljø på jeres respektive computere, således I er klar til at arbejde og udvikle selvstændigt videre med de komponenter som er til rådighed for jer. I er selvfølgelig mere end velkommen til selv at supplere med egne komponenter eller elementer som I måtte have.

## Hardware - indhold i pakken
### MCU
Det centrale element er selvfølgelig den microcontroller som vi vil forsøge, at få til at gøre det vi gerne vil have den til. Vi vil programmere den og dermed udvikle et embedded system. Vi skal selvfølgelig koble en række komponenter på for at udvide mulighederne for både input og output.

### Breadboard
Et Breadboard er en af ​​de mest grundlæggende stykker, når man lærer, hvordan man bygger kredsløb.
(altså et brødbræt eller skærebræt som vi kalder dem på dansk. Udtrykket kommer fra elektronikkens tidlige dage, hvor folk bogstaveligt talt ville føre søm eller skruer ind i træplader, som fx. skærebrætter på for at forbinde deres kredsløb.)
Et elektronisk breadboard (i modsætning til den type, hvor der fremstilles sandwich på) henviser faktisk til et loddet bræt. Disse er rigtig gode enheder til at lave midlertidige kredsløb og prototyper på, og de kræver absolut ingen lodning, altså kan man vente lidt med at anskaffe sig en loddekolbe.
Prototyping er processen hvor vi afprøver en idé ved at oprette en midlertidig model, hvorfra vi kan udvikle og forme andre systemer, og det er en af ​​de mest almindelige anvendelser af et breadboard. Hvis vi ikke er sikker på, hvordan et kredsløb vil reagere eller om et system virker efter hensigten, er det bedst først at opbygge en prototype og teste den.
